// Run by EBASH
// Not intended to be run individually
var chalk
try {
  chalk=require('chalk');
} catch (e) {

}
if (!chalk || !chalk.red) {
  console.log("Cannot find chalk. Exiting!")
  process.exit(1)
}

console.log(chalk.blue("---- EBash ----"));
console.log(chalk.blue("--- NJS Help ---"));
console.log(chalk.blue("NJS Commands are stored as simple node applications/modules in the node_commands folder in the ebash folder (defaulted to /usr/share/ebash - not viewable from node)"));
console.log(chalk.green("They can do anything nodejs-y. (they do after all, run in ")+chalk.rgb(50,255,255)("nodejs")+chalk.green(")\n\n"))
console.log(chalk.rgb(50,150,250)(chalk.rgb(255,0,0)("NOTE:")+" NJS Applications take a bit longer to start as node needs to be started every time they run.\n\n"))
